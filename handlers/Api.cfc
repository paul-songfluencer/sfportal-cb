component extends="coldbox.system.EventHandler" renderdata="json" {

    property name="campaignService" inject="CampaignService";
    property name="utilService" inject="UtilService";

    function index(event, rc, prc) {
        local.qCampaign = campaignService.load(1);
        var campaign = utilService.queryToArray(local.qCampaign)

        return campaign;
    }

}