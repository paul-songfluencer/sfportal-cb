component accessors="true" singleton {

    property name="aws" inject="aws@awscfml";

    public string function getFullTableName(required string name) {
        var tableName = "";

        local.preffyTables = aws.dynamodb.listTables();

        for (var table in local.preffyTables.data.tableNames) {
            if (findNoCase(arguments.name, table)) {
                local.tableName = table;
                break;
            }
        }

        return tableName;
    }

    public array function scanTableWithFilter(
        required string tableName
        , string filter=""
        , struct names={}
        , struct values={}) {

        var results = [];

        local.keysRemain = true;
        local.nextKey = {};
        while(local.keysRemain) {
            local.scanResults = aws.dynamodb.scan(
                tableName=arguments.tableName
                , filterExpression=arguments.filter
                , expressionAttributeNames=arguments.names
                , expressionAttributeValues=arguments.values
                , exclusiveStartKey = local.nextKey);

            if (arrayLen(local.scanResults.data.items)) {
                for (var item in local.scanResults.data.items) {
                    arrayAppend(results, item);
                }
            }

            if (structKeyExists(local.scanResults.data, "lastEvaluatedKey")) {
                local.nextKey = local.scanResults.data.lastEvaluatedKey;
            } else {
                local.keysRemain = false;
            }
        }

        return results;
    }

    public array function queryTableWithFilter(
        required string tableName
        , string indexName=""
        , string keyCondition=""
        , string filter=""
        , struct names={}
        , struct values={}) {

        var results = [];

        local.keysRemain = true;
        local.nextKey = {};
        while(local.keysRemain) {
            local.queryResults = aws.dynamodb.query(
                tableName=arguments.tableName
                , indexName=arguments.indexName
                , keyConditionExpression=arguments.keyCondition
                , expressionAttributeNames=arguments.names
                , expressionAttributeValues=arguments.values
                , exclusiveStartKey = local.nextKey);

            if (arrayLen(local.queryResults.data.items)) {
                for (var item in local.queryResults.data.items) {
                    arrayAppend(results, item);
                }
            }

            if (structKeyExists(local.queryResults.data, "lastEvaluatedKey")) {
                local.nextKey = local.queryResults.data.lastEvaluatedKey;
            } else {
                local.keysRemain = false;
            }
        }

        return results;
    }

    public void function putItemInTable(required string tableName, required struct item) {
        aws.dynamodb.putItem(
            TableName: arguments.tableName
            , Item: arguments.item);

        return;
    }
}