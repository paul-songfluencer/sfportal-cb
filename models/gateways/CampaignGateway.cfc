<cfcomponent accessors="true" singleton>

    <cffunction name="loadAll" returntype="query" output="false">
        <cfquery name="local.qLoadAll">
            SELECT
                CampaignID
                , UID
                , CampaignDivisionID
                , CampaignTypeID
                , ContactID
                , SalesManager_ContactID
                , CampaignManager_ContactID
                , SalesTerritoryID
                , PointofContact_ContactIDList
                , CampaignTitle
                , CampaignStatusID
                , CampaignStatus_UpdatedDateTime
                , CampaignStatus_UpdatedBy_ContactID
                , Campaign_PlatformID
                , ContactName_Primary
                , Phone_Primary
                , EmailAddress_Primary
                , ContactName_Secondary
                , Phone_Secondary
                , EmailAddress_Secondary
                , AdditionalEmailsList_ResultsLink
                , AdditionalEmailsList_NewPostNotification
                , ArtistName
                , ArtistWebsiteURL
                , ArtistSocialMediaHandle
                , CurrentSocialMediaFollowers
                , ReceiveDirectMessageNotifications
                , RecordLabel_ContactID
                , PublicistInfo
                , RadioPromoTeamInfo
                , SongTitle
                , TikTokAudioOptionID
                , TikTokAudioLinkURL
                , GenreIDList
                , KeyWordIDList
                , ReleaseDate
                , CampaignDurationID
                , DestinationID
                , DestinationURL
                , DestinationURL_CurrentNumber
                , Scrape_Results_URL
                , SocialMedia_UniqueID
                , URLErrorReasonID
                , ProxyCountryCode
                , SocialMedia_ImageURL
                , BitlyLinkURL
                , CampaignNotes_FromClient
                , Waiver1Agree
                , Waiver2Agree
                , MP3FileLocation
                , MP3URLLocation
                , AudioTimeStamp
                , StartDate_ClientRequested
                , EndDate_ClientRequested
                , StartDate_Scheduled
                , EndDate_Scheduled
                , StartDate_Actual
                , EndDate_Actual
                , StartDate_UpdatedBy_ContactID
                , EndDate_UpdatedBy_ContactID
                , Campaign_FirstPost_DateTime
                , Campaign_FirstPost_ClientNotified_DateTime
                , Campaign_FirstPost_ClientNotified_Email
                , CampaignViews_Target
                , NumberOfInfluencers_Target
                , MinimumInflunecersTrigger
                , OnboardingCallDetails
                , CreativeDirectionNotes_FromStaff
                , CreativeDirectionNotes_FromClient
                , PaymentStatusID
                , PaymentStatus_UpdatedContactID
                , PaymentStatus_UpdatedDateTime
                , ClientPayment_InvoiceAmount
                , ClientPayment_InvoiceDate
                , ClientPayment_InvoiceNumber
                , ClientPayment_PurchaseOrderNumber
                , ClientPayment_PaymentDate
                , ClientPayment_PaymentReferenceNumber
                , ClientPayment_PaymentMethodID
                , ClientPayment_BillingDetails
                , ClientPayment_BillingNotes
                , IsTest
                , InfluencerStaffApproval_EmailSent_DateTime
                , InfluencerStaffApproval_EmailSent_To_ContactID
                , InfluencerStaffApproval_EmailSent_From_ContactID
                , StaffCommentsForClientReport
                , PostInvite_Expiry_Days
                , PostNotify_Expiry_Days
                , PostResults_NotBefore_Days
                , ArchiveCampaign_NotBefore_Days
                , ArchiveCampaign_After_Date
                , ScrapeResultsYesNo
                , Chartmetric_ArtistID
                , Chartmetric_TrackID
                , Chartmetric_AlbumID
                , Spotify_URL
                , Score_ELO_UpdatedDateTime
                , SendCampaignReport
                , InfluencerSpendPercent
                , InfluencerSpendBudget
                , InfluencerSpendBudgetNotes
                , CompetitionOptionID
                , Competition_PrizeAmount
                , Competition_Deadline
                , IsCarveoutCampaign
                , ShowInfluencerCountry
                , RequestBoostCode
                , CreatedDateTime
                , CreatedContactID
                , ModifiedDateTime
                , ModifiedContactID
            FROM
                songfluencer.tblcampaign;
        </cfquery>

    <cfreturn local.qLoadAll />
    </cffunction>

    <cffunction name="load" returntype="query" output="false">
        <cfargument name="CampaignID" type="numeric" required="true" />

        <cfquery name="local.qLoad">
            SELECT
                CampaignID
                , UID
                , CampaignDivisionID
                , CampaignTypeID
                , ContactID
                , SalesManager_ContactID
                , CampaignManager_ContactID
                , SalesTerritoryID
                , PointofContact_ContactIDList
                , CampaignTitle
                , CampaignStatusID
                , CampaignStatus_UpdatedDateTime
                , CampaignStatus_UpdatedBy_ContactID
                , Campaign_PlatformID
                , ContactName_Primary
                , Phone_Primary
                , EmailAddress_Primary
                , ContactName_Secondary
                , Phone_Secondary
                , EmailAddress_Secondary
                , AdditionalEmailsList_ResultsLink
                , AdditionalEmailsList_NewPostNotification
                , ArtistName
                , ArtistWebsiteURL
                , ArtistSocialMediaHandle
                , CurrentSocialMediaFollowers
                , ReceiveDirectMessageNotifications
                , RecordLabel_ContactID
                , PublicistInfo
                , RadioPromoTeamInfo
                , SongTitle
                , TikTokAudioOptionID
                , TikTokAudioLinkURL
                , GenreIDList
                , KeyWordIDList
                , ReleaseDate
                , CampaignDurationID
                , DestinationID
                , DestinationURL
                , DestinationURL_CurrentNumber
                , Scrape_Results_URL
                , SocialMedia_UniqueID
                , URLErrorReasonID
                , ProxyCountryCode
                , SocialMedia_ImageURL
                , BitlyLinkURL
                , CampaignNotes_FromClient
                , Waiver1Agree
                , Waiver2Agree
                , MP3FileLocation
                , MP3URLLocation
                , AudioTimeStamp
                , StartDate_ClientRequested
                , EndDate_ClientRequested
                , StartDate_Scheduled
                , EndDate_Scheduled
                , StartDate_Actual
                , EndDate_Actual
                , StartDate_UpdatedBy_ContactID
                , EndDate_UpdatedBy_ContactID
                , Campaign_FirstPost_DateTime
                , Campaign_FirstPost_ClientNotified_DateTime
                , Campaign_FirstPost_ClientNotified_Email
                , CampaignViews_Target
                , NumberOfInfluencers_Target
                , MinimumInflunecersTrigger
                , OnboardingCallDetails
                , CreativeDirectionNotes_FromStaff
                , CreativeDirectionNotes_FromClient
                , PaymentStatusID
                , PaymentStatus_UpdatedContactID
                , PaymentStatus_UpdatedDateTime
                , ClientPayment_InvoiceAmount
                , ClientPayment_InvoiceDate
                , ClientPayment_InvoiceNumber
                , ClientPayment_PurchaseOrderNumber
                , ClientPayment_PaymentDate
                , ClientPayment_PaymentReferenceNumber
                , ClientPayment_PaymentMethodID
                , ClientPayment_BillingDetails
                , ClientPayment_BillingNotes
                , IsTest
                , InfluencerStaffApproval_EmailSent_DateTime
                , InfluencerStaffApproval_EmailSent_To_ContactID
                , InfluencerStaffApproval_EmailSent_From_ContactID
                , StaffCommentsForClientReport
                , PostInvite_Expiry_Days
                , PostNotify_Expiry_Days
                , PostResults_NotBefore_Days
                , ArchiveCampaign_NotBefore_Days
                , ArchiveCampaign_After_Date
                , ScrapeResultsYesNo
                , Chartmetric_ArtistID
                , Chartmetric_TrackID
                , Chartmetric_AlbumID
                , Spotify_URL
                , Score_ELO_UpdatedDateTime
                , SendCampaignReport
                , InfluencerSpendPercent
                , InfluencerSpendBudget
                , InfluencerSpendBudgetNotes
                , CompetitionOptionID
                , Competition_PrizeAmount
                , Competition_Deadline
                , IsCarveoutCampaign
                , ShowInfluencerCountry
                , RequestBoostCode
                , CreatedDateTime
                , CreatedContactID
                , ModifiedDateTime
                , ModifiedContactID
            FROM
                songfluencer.tblcampaign
            WHERE
                CampaignID = <cfqueryparam cfsqltype="CF_SQL_INTEGER" value="#arguments.CampaignID#" />
        </cfquery>

    <cfreturn local.qLoad />
    </cffunction>

    <cffunction name="insert" returntype="numeric" output="false">
        <cfargument name="UID" type="string" required="true" />
        <cfargument name="CampaignDivisionID" type="numeric" required="true" />
        <cfargument name="CampaignTypeID" type="numeric" required="true" />
        <cfargument name="ContactID" type="numeric" required="true" />
        <cfargument name="SalesManager_ContactID" type="numeric" required="true" />
        <cfargument name="CampaignManager_ContactID" type="numeric" required="true" />
        <cfargument name="SalesTerritoryID" type="numeric" required="true" />
        <cfargument name="PointofContact_ContactIDList" type="string" required="true" />
        <cfargument name="CampaignTitle" type="string" required="true" />
        <cfargument name="CampaignStatusID" type="numeric" required="true" />
        <cfargument name="CampaignStatus_UpdatedDateTime" type="string" required="true" />
        <cfargument name="CampaignStatus_UpdatedBy_ContactID" type="numeric" required="true" />
        <cfargument name="Campaign_PlatformID" type="numeric" required="true" />
        <cfargument name="ContactName_Primary" type="string" required="true" />
        <cfargument name="Phone_Primary" type="string" required="true" />
        <cfargument name="EmailAddress_Primary" type="string" required="true" />
        <cfargument name="ContactName_Secondary" type="string" required="true" />
        <cfargument name="Phone_Secondary" type="string" required="true" />
        <cfargument name="EmailAddress_Secondary" type="string" required="true" />
        <cfargument name="AdditionalEmailsList_ResultsLink" type="string" required="true" />
        <cfargument name="AdditionalEmailsList_NewPostNotification" type="string" required="true" />
        <cfargument name="ArtistName" type="string" required="true" />
        <cfargument name="ArtistWebsiteURL" type="string" required="true" />
        <cfargument name="ArtistSocialMediaHandle" type="string" required="true" />
        <cfargument name="CurrentSocialMediaFollowers" type="numeric" required="true" />
        <cfargument name="ReceiveDirectMessageNotifications" type="numeric" required="true" />
        <cfargument name="RecordLabel_ContactID" type="numeric" required="true" />
        <cfargument name="PublicistInfo" type="string" required="true" />
        <cfargument name="RadioPromoTeamInfo" type="string" required="true" />
        <cfargument name="SongTitle" type="string" required="true" />
        <cfargument name="TikTokAudioOptionID" type="numeric" required="true" />
        <cfargument name="TikTokAudioLinkURL" type="string" required="true" />
        <cfargument name="GenreIDList" type="string" required="true" />
        <cfargument name="KeyWordIDList" type="string" required="true" />
        <cfargument name="ReleaseDate" type="string" required="true" />
        <cfargument name="CampaignDurationID" type="numeric" required="true" />
        <cfargument name="DestinationID" type="numeric" required="true" />
        <cfargument name="DestinationURL" type="string" required="true" />
        <cfargument name="DestinationURL_CurrentNumber" type="numeric" required="true" />
        <cfargument name="Scrape_Results_URL" type="string" required="true" />
        <cfargument name="SocialMedia_UniqueID" type="string" required="true" />
        <cfargument name="URLErrorReasonID" type="numeric" required="true" />
        <cfargument name="ProxyCountryCode" type="string" required="true" />
        <cfargument name="SocialMedia_ImageURL" type="string" required="true" />
        <cfargument name="BitlyLinkURL" type="string" required="true" />
        <cfargument name="CampaignNotes_FromClient" type="string" required="true" />
        <cfargument name="Waiver1Agree" type="numeric" required="true" />
        <cfargument name="Waiver2Agree" type="numeric" required="true" />
        <cfargument name="MP3FileLocation" type="string" required="true" />
        <cfargument name="MP3URLLocation" type="string" required="true" />
        <cfargument name="AudioTimeStamp" type="string" required="true" />
        <cfargument name="StartDate_ClientRequested" type="string" required="true" />
        <cfargument name="EndDate_ClientRequested" type="string" required="true" />
        <cfargument name="StartDate_Scheduled" type="string" required="true" />
        <cfargument name="EndDate_Scheduled" type="string" required="true" />
        <cfargument name="StartDate_Actual" type="string" required="true" />
        <cfargument name="EndDate_Actual" type="string" required="true" />
        <cfargument name="StartDate_UpdatedBy_ContactID" type="numeric" required="true" />
        <cfargument name="EndDate_UpdatedBy_ContactID" type="numeric" required="true" />
        <cfargument name="Campaign_FirstPost_DateTime" type="string" required="true" />
        <cfargument name="Campaign_FirstPost_ClientNotified_DateTime" type="string" required="true" />
        <cfargument name="Campaign_FirstPost_ClientNotified_Email" type="string" required="true" />
        <cfargument name="CampaignViews_Target" type="numeric" required="true" />
        <cfargument name="NumberOfInfluencers_Target" type="string" required="true" />
        <cfargument name="MinimumInflunecersTrigger" type="numeric" required="true" />
        <cfargument name="OnboardingCallDetails" type="string" required="true" />
        <cfargument name="CreativeDirectionNotes_FromStaff" type="string" required="true" />
        <cfargument name="CreativeDirectionNotes_FromClient" type="string" required="true" />
        <cfargument name="PaymentStatusID" type="numeric" required="true" />
        <cfargument name="PaymentStatus_UpdatedContactID" type="numeric" required="true" />
        <cfargument name="PaymentStatus_UpdatedDateTime" type="string" required="true" />
        <cfargument name="ClientPayment_InvoiceAmount" type="numeric" required="true" />
        <cfargument name="ClientPayment_InvoiceDate" type="string" required="true" />
        <cfargument name="ClientPayment_InvoiceNumber" type="string" required="true" />
        <cfargument name="ClientPayment_PurchaseOrderNumber" type="string" required="true" />
        <cfargument name="ClientPayment_PaymentDate" type="string" required="true" />
        <cfargument name="ClientPayment_PaymentReferenceNumber" type="string" required="true" />
        <cfargument name="ClientPayment_PaymentMethodID" type="numeric" required="true" />
        <cfargument name="ClientPayment_BillingDetails" type="string" required="true" />
        <cfargument name="ClientPayment_BillingNotes" type="string" required="true" />
        <cfargument name="IsTest" type="numeric" required="true" />
        <cfargument name="InfluencerStaffApproval_EmailSent_DateTime" type="string" required="true" />
        <cfargument name="InfluencerStaffApproval_EmailSent_To_ContactID" type="numeric" required="true" />
        <cfargument name="InfluencerStaffApproval_EmailSent_From_ContactID" type="numeric" required="true" />
        <cfargument name="StaffCommentsForClientReport" type="string" required="true" />
        <cfargument name="PostInvite_Expiry_Days" type="numeric" required="true" />
        <cfargument name="PostNotify_Expiry_Days" type="numeric" required="true" />
        <cfargument name="PostResults_NotBefore_Days" type="numeric" required="true" />
        <cfargument name="ArchiveCampaign_NotBefore_Days" type="numeric" required="true" />
        <cfargument name="ArchiveCampaign_After_Date" type="string" required="true" />
        <cfargument name="ScrapeResultsYesNo" type="numeric" required="true" />
        <cfargument name="Chartmetric_ArtistID" type="string" required="true" />
        <cfargument name="Chartmetric_TrackID" type="string" required="true" />
        <cfargument name="Chartmetric_AlbumID" type="string" required="true" />
        <cfargument name="Spotify_URL" type="string" required="true" />
        <cfargument name="Score_ELO_UpdatedDateTime" type="string" required="true" />
        <cfargument name="SendCampaignReport" type="numeric" required="true" />
        <cfargument name="InfluencerSpendPercent" type="numeric" required="true" />
        <cfargument name="InfluencerSpendBudget" type="numeric" required="true" />
        <cfargument name="InfluencerSpendBudgetNotes" type="string" required="true" />
        <cfargument name="CompetitionOptionID" type="numeric" required="true" />
        <cfargument name="Competition_PrizeAmount" type="numeric" required="true" />
        <cfargument name="Competition_Deadline" type="string" required="true" />
        <cfargument name="IsCarveoutCampaign" type="numeric" required="true" />
        <cfargument name="ShowInfluencerCountry" type="numeric" required="true" />
        <cfargument name="RequestBoostCode" type="numeric" required="true" />
        <cfargument name="CreatedDateTime" type="string" required="true" />
        <cfargument name="CreatedContactID" type="numeric" required="true" />
        <cfargument name="ModifiedDateTime" type="string" required="true" />
        <cfargument name="ModifiedContactID" type="numeric" required="true" />

        <cfquery name="local.qInsert" datasource="songfluencer">
            INSERT INTO songfluencer.tblcampaign (
                UID
                , CampaignDivisionID
                , CampaignTypeID
                , ContactID
                , SalesManager_ContactID
                , CampaignManager_ContactID
                , SalesTerritoryID
                , PointofContact_ContactIDList
                , CampaignTitle
                , CampaignStatusID
                , CampaignStatus_UpdatedDateTime
                , CampaignStatus_UpdatedBy_ContactID
                , Campaign_PlatformID
                , ContactName_Primary
                , Phone_Primary
                , EmailAddress_Primary
                , ContactName_Secondary
                , Phone_Secondary
                , EmailAddress_Secondary
                , AdditionalEmailsList_ResultsLink
                , AdditionalEmailsList_NewPostNotification
                , ArtistName
                , ArtistWebsiteURL
                , ArtistSocialMediaHandle
                , CurrentSocialMediaFollowers
                , ReceiveDirectMessageNotifications
                , RecordLabel_ContactID
                , PublicistInfo
                , RadioPromoTeamInfo
                , SongTitle
                , TikTokAudioOptionID
                , TikTokAudioLinkURL
                , GenreIDList
                , KeyWordIDList
                , ReleaseDate
                , CampaignDurationID
                , DestinationID
                , DestinationURL
                , DestinationURL_CurrentNumber
                , Scrape_Results_URL
                , SocialMedia_UniqueID
                , URLErrorReasonID
                , ProxyCountryCode
                , SocialMedia_ImageURL
                , BitlyLinkURL
                , CampaignNotes_FromClient
                , Waiver1Agree
                , Waiver2Agree
                , MP3FileLocation
                , MP3URLLocation
                , AudioTimeStamp
                , StartDate_ClientRequested
                , EndDate_ClientRequested
                , StartDate_Scheduled
                , EndDate_Scheduled
                , StartDate_Actual
                , EndDate_Actual
                , StartDate_UpdatedBy_ContactID
                , EndDate_UpdatedBy_ContactID
                , Campaign_FirstPost_DateTime
                , Campaign_FirstPost_ClientNotified_DateTime
                , Campaign_FirstPost_ClientNotified_Email
                , CampaignViews_Target
                , NumberOfInfluencers_Target
                , MinimumInflunecersTrigger
                , OnboardingCallDetails
                , CreativeDirectionNotes_FromStaff
                , CreativeDirectionNotes_FromClient
                , PaymentStatusID
                , PaymentStatus_UpdatedContactID
                , PaymentStatus_UpdatedDateTime
                , ClientPayment_InvoiceAmount
                , ClientPayment_InvoiceDate
                , ClientPayment_InvoiceNumber
                , ClientPayment_PurchaseOrderNumber
                , ClientPayment_PaymentDate
                , ClientPayment_PaymentReferenceNumber
                , ClientPayment_PaymentMethodID
                , ClientPayment_BillingDetails
                , ClientPayment_BillingNotes
                , IsTest
                , InfluencerStaffApproval_EmailSent_DateTime
                , InfluencerStaffApproval_EmailSent_To_ContactID
                , InfluencerStaffApproval_EmailSent_From_ContactID
                , StaffCommentsForClientReport
                , PostInvite_Expiry_Days
                , PostNotify_Expiry_Days
                , PostResults_NotBefore_Days
                , ArchiveCampaign_NotBefore_Days
                , ArchiveCampaign_After_Date
                , ScrapeResultsYesNo
                , Chartmetric_ArtistID
                , Chartmetric_TrackID
                , Chartmetric_AlbumID
                , Spotify_URL
                , Score_ELO_UpdatedDateTime
                , SendCampaignReport
                , InfluencerSpendPercent
                , InfluencerSpendBudget
                , InfluencerSpendBudgetNotes
                , CompetitionOptionID
                , Competition_PrizeAmount
                , Competition_Deadline
                , IsCarveoutCampaign
                , ShowInfluencerCountry
                , RequestBoostCode
                , CreatedDateTime
                , CreatedContactID
                , ModifiedDateTime
                , ModifiedContactID
            ) VALUES (
                <cfqueryparam cfsqltype="CF_SQL_VARCHAR" value="#arguments.UID#" />
                , <cfqueryparam cfsqltype="CF_SQL_INTEGER" value="#arguments.CampaignDivisionID#" />
                , <cfqueryparam cfsqltype="CF_SQL_INTEGER" value="#arguments.CampaignTypeID#" />
                , <cfqueryparam cfsqltype="CF_SQL_INTEGER" value="#arguments.ContactID#" />
                , <cfqueryparam cfsqltype="CF_SQL_INTEGER" value="#arguments.SalesManager_ContactID#" />
                , <cfqueryparam cfsqltype="CF_SQL_INTEGER" value="#arguments.CampaignManager_ContactID#" />
                , <cfqueryparam cfsqltype="CF_SQL_INTEGER" value="#arguments.SalesTerritoryID#" />
                , <cfqueryparam cfsqltype="CF_SQL_VARCHAR" value="#arguments.PointofContact_ContactIDList#" />
                , <cfqueryparam cfsqltype="CF_SQL_VARCHAR" value="#arguments.CampaignTitle#" />
                , <cfqueryparam cfsqltype="CF_SQL_INTEGER" value="#arguments.CampaignStatusID#" />
                , <cfqueryparam cfsqltype="CF_SQL_TIMESTAMP" value="#arguments.CampaignStatus_UpdatedDateTime#" />
                , <cfqueryparam cfsqltype="CF_SQL_INTEGER" value="#arguments.CampaignStatus_UpdatedBy_ContactID#" />
                , <cfqueryparam cfsqltype="CF_SQL_INTEGER" value="#arguments.Campaign_PlatformID#" />
                , <cfqueryparam cfsqltype="CF_SQL_VARCHAR" value="#arguments.ContactName_Primary#" />
                , <cfqueryparam cfsqltype="CF_SQL_VARCHAR" value="#arguments.Phone_Primary#" />
                , <cfqueryparam cfsqltype="CF_SQL_VARCHAR" value="#arguments.EmailAddress_Primary#" />
                , <cfqueryparam cfsqltype="CF_SQL_VARCHAR" value="#arguments.ContactName_Secondary#" />
                , <cfqueryparam cfsqltype="CF_SQL_VARCHAR" value="#arguments.Phone_Secondary#" />
                , <cfqueryparam cfsqltype="CF_SQL_VARCHAR" value="#arguments.EmailAddress_Secondary#" />
                , <cfqueryparam cfsqltype="CF_SQL_VARCHAR" value="#arguments.AdditionalEmailsList_ResultsLink#" />
                , <cfqueryparam cfsqltype="CF_SQL_VARCHAR" value="#arguments.AdditionalEmailsList_NewPostNotification#" />
                , <cfqueryparam cfsqltype="CF_SQL_VARCHAR" value="#arguments.ArtistName#" />
                , <cfqueryparam cfsqltype="CF_SQL_VARCHAR" value="#arguments.ArtistWebsiteURL#" />
                , <cfqueryparam cfsqltype="CF_SQL_VARCHAR" value="#arguments.ArtistSocialMediaHandle#" />
                , <cfqueryparam cfsqltype="CF_SQL_INTEGER" value="#arguments.CurrentSocialMediaFollowers#" />
                , <cfqueryparam cfsqltype="CF_SQL_TINYINT" value="#arguments.ReceiveDirectMessageNotifications#" />
                , <cfqueryparam cfsqltype="CF_SQL_INTEGER" value="#arguments.RecordLabel_ContactID#" />
                , <cfqueryparam cfsqltype="CF_SQL_VARCHAR" value="#arguments.PublicistInfo#" />
                , <cfqueryparam cfsqltype="CF_SQL_VARCHAR" value="#arguments.RadioPromoTeamInfo#" />
                , <cfqueryparam cfsqltype="CF_SQL_VARCHAR" value="#arguments.SongTitle#" />
                , <cfqueryparam cfsqltype="CF_SQL_INTEGER" value="#arguments.TikTokAudioOptionID#" />
                , <cfqueryparam cfsqltype="CF_SQL_VARCHAR" value="#arguments.TikTokAudioLinkURL#" />
                , <cfqueryparam cfsqltype="CF_SQL_VARCHAR" value="#arguments.GenreIDList#" />
                , <cfqueryparam cfsqltype="CF_SQL_VARCHAR" value="#arguments.KeyWordIDList#" />
                , <cfqueryparam cfsqltype="CF_SQL_DATE" value="#arguments.ReleaseDate#" />
                , <cfqueryparam cfsqltype="CF_SQL_INTEGER" value="#arguments.CampaignDurationID#" />
                , <cfqueryparam cfsqltype="CF_SQL_INTEGER" value="#arguments.DestinationID#" />
                , <cfqueryparam cfsqltype="CF_SQL_VARCHAR" value="#arguments.DestinationURL#" />
                , <cfqueryparam cfsqltype="CF_SQL_INTEGER" value="#arguments.DestinationURL_CurrentNumber#" />
                , <cfqueryparam cfsqltype="CF_SQL_VARCHAR" value="#arguments.Scrape_Results_URL#" />
                , <cfqueryparam cfsqltype="CF_SQL_VARCHAR" value="#arguments.SocialMedia_UniqueID#" />
                , <cfqueryparam cfsqltype="CF_SQL_INTEGER" value="#arguments.URLErrorReasonID#" />
                , <cfqueryparam cfsqltype="CF_SQL_VARCHAR" value="#arguments.ProxyCountryCode#" />
                , <cfqueryparam cfsqltype="CF_SQL_VARCHAR" value="#arguments.SocialMedia_ImageURL#" />
                , <cfqueryparam cfsqltype="CF_SQL_VARCHAR" value="#arguments.BitlyLinkURL#" />
                , <cfqueryparam cfsqltype="CF_SQL_VARCHAR" value="#arguments.CampaignNotes_FromClient#" />
                , <cfqueryparam cfsqltype="CF_SQL_TINYINT" value="#arguments.Waiver1Agree#" />
                , <cfqueryparam cfsqltype="CF_SQL_TINYINT" value="#arguments.Waiver2Agree#" />
                , <cfqueryparam cfsqltype="CF_SQL_VARCHAR" value="#arguments.MP3FileLocation#" />
                , <cfqueryparam cfsqltype="CF_SQL_VARCHAR" value="#arguments.MP3URLLocation#" />
                , <cfqueryparam cfsqltype="CF_SQL_VARCHAR" value="#arguments.AudioTimeStamp#" />
                , <cfqueryparam cfsqltype="CF_SQL_DATE" value="#arguments.StartDate_ClientRequested#" />
                , <cfqueryparam cfsqltype="CF_SQL_DATE" value="#arguments.EndDate_ClientRequested#" />
                , <cfqueryparam cfsqltype="CF_SQL_DATE" value="#arguments.StartDate_Scheduled#" />
                , <cfqueryparam cfsqltype="CF_SQL_DATE" value="#arguments.EndDate_Scheduled#" />
                , <cfqueryparam cfsqltype="CF_SQL_DATE" value="#arguments.StartDate_Actual#" />
                , <cfqueryparam cfsqltype="CF_SQL_DATE" value="#arguments.EndDate_Actual#" />
                , <cfqueryparam cfsqltype="CF_SQL_INTEGER" value="#arguments.StartDate_UpdatedBy_ContactID#" />
                , <cfqueryparam cfsqltype="CF_SQL_INTEGER" value="#arguments.EndDate_UpdatedBy_ContactID#" />
                , <cfqueryparam cfsqltype="CF_SQL_TIMESTAMP" value="#arguments.Campaign_FirstPost_DateTime#" />
                , <cfqueryparam cfsqltype="CF_SQL_TIMESTAMP" value="#arguments.Campaign_FirstPost_ClientNotified_DateTime#" />
                , <cfqueryparam cfsqltype="CF_SQL_VARCHAR" value="#arguments.Campaign_FirstPost_ClientNotified_Email#" />
                , <cfqueryparam cfsqltype="CF_SQL_INTEGER" value="#arguments.CampaignViews_Target#" />
                , <cfqueryparam cfsqltype="CF_SQL_VARCHAR" value="#arguments.NumberOfInfluencers_Target#" />
                , <cfqueryparam cfsqltype="CF_SQL_INTEGER" value="#arguments.MinimumInflunecersTrigger#" />
                , <cfqueryparam cfsqltype="CF_SQL_VARCHAR" value="#arguments.OnboardingCallDetails#" />
                , <cfqueryparam cfsqltype="CF_SQL_VARCHAR" value="#arguments.CreativeDirectionNotes_FromStaff#" />
                , <cfqueryparam cfsqltype="CF_SQL_VARCHAR" value="#arguments.CreativeDirectionNotes_FromClient#" />
                , <cfqueryparam cfsqltype="CF_SQL_INTEGER" value="#arguments.PaymentStatusID#" />
                , <cfqueryparam cfsqltype="CF_SQL_INTEGER" value="#arguments.PaymentStatus_UpdatedContactID#" />
                , <cfqueryparam cfsqltype="CF_SQL_TIMESTAMP" value="#arguments.PaymentStatus_UpdatedDateTime#" />
                , <cfqueryparam cfsqltype="CF_SQL_DECIMAL" value="#arguments.ClientPayment_InvoiceAmount#" />
                , <cfqueryparam cfsqltype="CF_SQL_DATE" value="#arguments.ClientPayment_InvoiceDate#" />
                , <cfqueryparam cfsqltype="CF_SQL_VARCHAR" value="#arguments.ClientPayment_InvoiceNumber#" />
                , <cfqueryparam cfsqltype="CF_SQL_VARCHAR" value="#arguments.ClientPayment_PurchaseOrderNumber#" />
                , <cfqueryparam cfsqltype="CF_SQL_DATE" value="#arguments.ClientPayment_PaymentDate#" />
                , <cfqueryparam cfsqltype="CF_SQL_VARCHAR" value="#arguments.ClientPayment_PaymentReferenceNumber#" />
                , <cfqueryparam cfsqltype="CF_SQL_INTEGER" value="#arguments.ClientPayment_PaymentMethodID#" />
                , <cfqueryparam cfsqltype="CF_SQL_VARCHAR" value="#arguments.ClientPayment_BillingDetails#" />
                , <cfqueryparam cfsqltype="CF_SQL_VARCHAR" value="#arguments.ClientPayment_BillingNotes#" />
                , <cfqueryparam cfsqltype="CF_SQL_TINYINT" value="#arguments.IsTest#" />
                , <cfqueryparam cfsqltype="CF_SQL_TIMESTAMP" value="#arguments.InfluencerStaffApproval_EmailSent_DateTime#" />
                , <cfqueryparam cfsqltype="CF_SQL_INTEGER" value="#arguments.InfluencerStaffApproval_EmailSent_To_ContactID#" />
                , <cfqueryparam cfsqltype="CF_SQL_INTEGER" value="#arguments.InfluencerStaffApproval_EmailSent_From_ContactID#" />
                , <cfqueryparam cfsqltype="CF_SQL_VARCHAR" value="#arguments.StaffCommentsForClientReport#" />
                , <cfqueryparam cfsqltype="CF_SQL_INTEGER" value="#arguments.PostInvite_Expiry_Days#" />
                , <cfqueryparam cfsqltype="CF_SQL_INTEGER" value="#arguments.PostNotify_Expiry_Days#" />
                , <cfqueryparam cfsqltype="CF_SQL_INTEGER" value="#arguments.PostResults_NotBefore_Days#" />
                , <cfqueryparam cfsqltype="CF_SQL_INTEGER" value="#arguments.ArchiveCampaign_NotBefore_Days#" />
                , <cfqueryparam cfsqltype="CF_SQL_DATE" value="#arguments.ArchiveCampaign_After_Date#" />
                , <cfqueryparam cfsqltype="CF_SQL_TINYINT" value="#arguments.ScrapeResultsYesNo#" />
                , <cfqueryparam cfsqltype="CF_SQL_VARCHAR" value="#arguments.Chartmetric_ArtistID#" />
                , <cfqueryparam cfsqltype="CF_SQL_VARCHAR" value="#arguments.Chartmetric_TrackID#" />
                , <cfqueryparam cfsqltype="CF_SQL_VARCHAR" value="#arguments.Chartmetric_AlbumID#" />
                , <cfqueryparam cfsqltype="CF_SQL_VARCHAR" value="#arguments.Spotify_URL#" />
                , <cfqueryparam cfsqltype="CF_SQL_TIMESTAMP" value="#arguments.Score_ELO_UpdatedDateTime#" />
                , <cfqueryparam cfsqltype="CF_SQL_TINYINT" value="#arguments.SendCampaignReport#" />
                , <cfqueryparam cfsqltype="CF_SQL_DECIMAL" value="#arguments.InfluencerSpendPercent#" />
                , <cfqueryparam cfsqltype="CF_SQL_DECIMAL" value="#arguments.InfluencerSpendBudget#" />
                , <cfqueryparam cfsqltype="CF_SQL_VARCHAR" value="#arguments.InfluencerSpendBudgetNotes#" />
                , <cfqueryparam cfsqltype="CF_SQL_INTEGER" value="#arguments.CompetitionOptionID#" />
                , <cfqueryparam cfsqltype="CF_SQL_DECIMAL" value="#arguments.Competition_PrizeAmount#" />
                , <cfqueryparam cfsqltype="CF_SQL_TIMESTAMP" value="#arguments.Competition_Deadline#" />
                , <cfqueryparam cfsqltype="CF_SQL_TINYINT" value="#arguments.IsCarveoutCampaign#" />
                , <cfqueryparam cfsqltype="CF_SQL_TINYINT" value="#arguments.ShowInfluencerCountry#" />
                , <cfqueryparam cfsqltype="CF_SQL_TINYINT" value="#arguments.RequestBoostCode#" />
                , <cfqueryparam cfsqltype="CF_SQL_TIMESTAMP" value="#arguments.CreatedDateTime#" />
                , <cfqueryparam cfsqltype="CF_SQL_INTEGER" value="#arguments.CreatedContactID#" />
                , <cfqueryparam cfsqltype="CF_SQL_TIMESTAMP" value="#arguments.ModifiedDateTime#" />
                , <cfqueryparam cfsqltype="CF_SQL_INTEGER" value="#arguments.ModifiedContactID#" />
            )
        </cfquery>

    <cfreturn local.qInsert.generatedKey />
    </cffunction>

    <cffunction name="update" returntype="void" output="false">
        <cfargument name="CampaignID" type="numeric" required="true" />
        <cfargument name="UID" type="string" required="true" />
        <cfargument name="CampaignDivisionID" type="numeric" required="true" />
        <cfargument name="CampaignTypeID" type="numeric" required="true" />
        <cfargument name="ContactID" type="numeric" required="true" />
        <cfargument name="SalesManager_ContactID" type="numeric" required="true" />
        <cfargument name="CampaignManager_ContactID" type="numeric" required="true" />
        <cfargument name="SalesTerritoryID" type="numeric" required="true" />
        <cfargument name="PointofContact_ContactIDList" type="string" required="true" />
        <cfargument name="CampaignTitle" type="string" required="true" />
        <cfargument name="CampaignStatusID" type="numeric" required="true" />
        <cfargument name="CampaignStatus_UpdatedDateTime" type="string" required="true" />
        <cfargument name="CampaignStatus_UpdatedBy_ContactID" type="numeric" required="true" />
        <cfargument name="Campaign_PlatformID" type="numeric" required="true" />
        <cfargument name="ContactName_Primary" type="string" required="true" />
        <cfargument name="Phone_Primary" type="string" required="true" />
        <cfargument name="EmailAddress_Primary" type="string" required="true" />
        <cfargument name="ContactName_Secondary" type="string" required="true" />
        <cfargument name="Phone_Secondary" type="string" required="true" />
        <cfargument name="EmailAddress_Secondary" type="string" required="true" />
        <cfargument name="AdditionalEmailsList_ResultsLink" type="string" required="true" />
        <cfargument name="AdditionalEmailsList_NewPostNotification" type="string" required="true" />
        <cfargument name="ArtistName" type="string" required="true" />
        <cfargument name="ArtistWebsiteURL" type="string" required="true" />
        <cfargument name="ArtistSocialMediaHandle" type="string" required="true" />
        <cfargument name="CurrentSocialMediaFollowers" type="numeric" required="true" />
        <cfargument name="ReceiveDirectMessageNotifications" type="numeric" required="true" />
        <cfargument name="RecordLabel_ContactID" type="numeric" required="true" />
        <cfargument name="PublicistInfo" type="string" required="true" />
        <cfargument name="RadioPromoTeamInfo" type="string" required="true" />
        <cfargument name="SongTitle" type="string" required="true" />
        <cfargument name="TikTokAudioOptionID" type="numeric" required="true" />
        <cfargument name="TikTokAudioLinkURL" type="string" required="true" />
        <cfargument name="GenreIDList" type="string" required="true" />
        <cfargument name="KeyWordIDList" type="string" required="true" />
        <cfargument name="ReleaseDate" type="string" required="true" />
        <cfargument name="CampaignDurationID" type="numeric" required="true" />
        <cfargument name="DestinationID" type="numeric" required="true" />
        <cfargument name="DestinationURL" type="string" required="true" />
        <cfargument name="DestinationURL_CurrentNumber" type="numeric" required="true" />
        <cfargument name="Scrape_Results_URL" type="string" required="true" />
        <cfargument name="SocialMedia_UniqueID" type="string" required="true" />
        <cfargument name="URLErrorReasonID" type="numeric" required="true" />
        <cfargument name="ProxyCountryCode" type="string" required="true" />
        <cfargument name="SocialMedia_ImageURL" type="string" required="true" />
        <cfargument name="BitlyLinkURL" type="string" required="true" />
        <cfargument name="CampaignNotes_FromClient" type="string" required="true" />
        <cfargument name="Waiver1Agree" type="numeric" required="true" />
        <cfargument name="Waiver2Agree" type="numeric" required="true" />
        <cfargument name="MP3FileLocation" type="string" required="true" />
        <cfargument name="MP3URLLocation" type="string" required="true" />
        <cfargument name="AudioTimeStamp" type="string" required="true" />
        <cfargument name="StartDate_ClientRequested" type="string" required="true" />
        <cfargument name="EndDate_ClientRequested" type="string" required="true" />
        <cfargument name="StartDate_Scheduled" type="string" required="true" />
        <cfargument name="EndDate_Scheduled" type="string" required="true" />
        <cfargument name="StartDate_Actual" type="string" required="true" />
        <cfargument name="EndDate_Actual" type="string" required="true" />
        <cfargument name="StartDate_UpdatedBy_ContactID" type="numeric" required="true" />
        <cfargument name="EndDate_UpdatedBy_ContactID" type="numeric" required="true" />
        <cfargument name="Campaign_FirstPost_DateTime" type="string" required="true" />
        <cfargument name="Campaign_FirstPost_ClientNotified_DateTime" type="string" required="true" />
        <cfargument name="Campaign_FirstPost_ClientNotified_Email" type="string" required="true" />
        <cfargument name="CampaignViews_Target" type="numeric" required="true" />
        <cfargument name="NumberOfInfluencers_Target" type="string" required="true" />
        <cfargument name="MinimumInflunecersTrigger" type="numeric" required="true" />
        <cfargument name="OnboardingCallDetails" type="string" required="true" />
        <cfargument name="CreativeDirectionNotes_FromStaff" type="string" required="true" />
        <cfargument name="CreativeDirectionNotes_FromClient" type="string" required="true" />
        <cfargument name="PaymentStatusID" type="numeric" required="true" />
        <cfargument name="PaymentStatus_UpdatedContactID" type="numeric" required="true" />
        <cfargument name="PaymentStatus_UpdatedDateTime" type="string" required="true" />
        <cfargument name="ClientPayment_InvoiceAmount" type="numeric" required="true" />
        <cfargument name="ClientPayment_InvoiceDate" type="string" required="true" />
        <cfargument name="ClientPayment_InvoiceNumber" type="string" required="true" />
        <cfargument name="ClientPayment_PurchaseOrderNumber" type="string" required="true" />
        <cfargument name="ClientPayment_PaymentDate" type="string" required="true" />
        <cfargument name="ClientPayment_PaymentReferenceNumber" type="string" required="true" />
        <cfargument name="ClientPayment_PaymentMethodID" type="numeric" required="true" />
        <cfargument name="ClientPayment_BillingDetails" type="string" required="true" />
        <cfargument name="ClientPayment_BillingNotes" type="string" required="true" />
        <cfargument name="IsTest" type="numeric" required="true" />
        <cfargument name="InfluencerStaffApproval_EmailSent_DateTime" type="string" required="true" />
        <cfargument name="InfluencerStaffApproval_EmailSent_To_ContactID" type="numeric" required="true" />
        <cfargument name="InfluencerStaffApproval_EmailSent_From_ContactID" type="numeric" required="true" />
        <cfargument name="StaffCommentsForClientReport" type="string" required="true" />
        <cfargument name="PostInvite_Expiry_Days" type="numeric" required="true" />
        <cfargument name="PostNotify_Expiry_Days" type="numeric" required="true" />
        <cfargument name="PostResults_NotBefore_Days" type="numeric" required="true" />
        <cfargument name="ArchiveCampaign_NotBefore_Days" type="numeric" required="true" />
        <cfargument name="ArchiveCampaign_After_Date" type="string" required="true" />
        <cfargument name="ScrapeResultsYesNo" type="numeric" required="true" />
        <cfargument name="Chartmetric_ArtistID" type="string" required="true" />
        <cfargument name="Chartmetric_TrackID" type="string" required="true" />
        <cfargument name="Chartmetric_AlbumID" type="string" required="true" />
        <cfargument name="Spotify_URL" type="string" required="true" />
        <cfargument name="Score_ELO_UpdatedDateTime" type="string" required="true" />
        <cfargument name="SendCampaignReport" type="numeric" required="true" />
        <cfargument name="InfluencerSpendPercent" type="numeric" required="true" />
        <cfargument name="InfluencerSpendBudget" type="numeric" required="true" />
        <cfargument name="InfluencerSpendBudgetNotes" type="string" required="true" />
        <cfargument name="CompetitionOptionID" type="numeric" required="true" />
        <cfargument name="Competition_PrizeAmount" type="numeric" required="true" />
        <cfargument name="Competition_Deadline" type="string" required="true" />
        <cfargument name="IsCarveoutCampaign" type="numeric" required="true" />
        <cfargument name="ShowInfluencerCountry" type="numeric" required="true" />
        <cfargument name="RequestBoostCode" type="numeric" required="true" />
        <cfargument name="CreatedDateTime" type="string" required="true" />
        <cfargument name="CreatedContactID" type="numeric" required="true" />
        <cfargument name="ModifiedDateTime" type="string" required="true" />
        <cfargument name="ModifiedContactID" type="numeric" required="true" />

        <cfquery name="local.qInsert" datasource="songfluencer">
            UPDATE songfluencer.tblcampaign
            SET
                UID = <cfqueryparam cfsqltype="CF_SQL_VARCHAR" value="#arguments.UID#" />
                , CampaignDivisionID = <cfqueryparam cfsqltype="CF_SQL_INTEGER" value="#arguments.CampaignDivisionID#" />
                , CampaignTypeID = <cfqueryparam cfsqltype="CF_SQL_INTEGER" value="#arguments.CampaignTypeID#" />
                , ContactID = <cfqueryparam cfsqltype="CF_SQL_INTEGER" value="#arguments.ContactID#" />
                , SalesManager_ContactID = <cfqueryparam cfsqltype="CF_SQL_INTEGER" value="#arguments.SalesManager_ContactID#" />
                , CampaignManager_ContactID = <cfqueryparam cfsqltype="CF_SQL_INTEGER" value="#arguments.CampaignManager_ContactID#" />
                , SalesTerritoryID = <cfqueryparam cfsqltype="CF_SQL_INTEGER" value="#arguments.SalesTerritoryID#" />
                , PointofContact_ContactIDList = <cfqueryparam cfsqltype="CF_SQL_VARCHAR" value="#arguments.PointofContact_ContactIDList#" />
                , CampaignTitle = <cfqueryparam cfsqltype="CF_SQL_VARCHAR" value="#arguments.CampaignTitle#" />
                , CampaignStatusID = <cfqueryparam cfsqltype="CF_SQL_INTEGER" value="#arguments.CampaignStatusID#" />
                , CampaignStatus_UpdatedDateTime = <cfqueryparam cfsqltype="CF_SQL_TIMESTAMP" value="#arguments.CampaignStatus_UpdatedDateTime#" />
                , CampaignStatus_UpdatedBy_ContactID = <cfqueryparam cfsqltype="CF_SQL_INTEGER" value="#arguments.CampaignStatus_UpdatedBy_ContactID#" />
                , Campaign_PlatformID = <cfqueryparam cfsqltype="CF_SQL_INTEGER" value="#arguments.Campaign_PlatformID#" />
                , ContactName_Primary = <cfqueryparam cfsqltype="CF_SQL_VARCHAR" value="#arguments.ContactName_Primary#" />
                , Phone_Primary = <cfqueryparam cfsqltype="CF_SQL_VARCHAR" value="#arguments.Phone_Primary#" />
                , EmailAddress_Primary = <cfqueryparam cfsqltype="CF_SQL_VARCHAR" value="#arguments.EmailAddress_Primary#" />
                , ContactName_Secondary = <cfqueryparam cfsqltype="CF_SQL_VARCHAR" value="#arguments.ContactName_Secondary#" />
                , Phone_Secondary = <cfqueryparam cfsqltype="CF_SQL_VARCHAR" value="#arguments.Phone_Secondary#" />
                , EmailAddress_Secondary = <cfqueryparam cfsqltype="CF_SQL_VARCHAR" value="#arguments.EmailAddress_Secondary#" />
                , AdditionalEmailsList_ResultsLink = <cfqueryparam cfsqltype="CF_SQL_VARCHAR" value="#arguments.AdditionalEmailsList_ResultsLink#" />
                , AdditionalEmailsList_NewPostNotification = <cfqueryparam cfsqltype="CF_SQL_VARCHAR" value="#arguments.AdditionalEmailsList_NewPostNotification#" />
                , ArtistName = <cfqueryparam cfsqltype="CF_SQL_VARCHAR" value="#arguments.ArtistName#" />
                , ArtistWebsiteURL = <cfqueryparam cfsqltype="CF_SQL_VARCHAR" value="#arguments.ArtistWebsiteURL#" />
                , ArtistSocialMediaHandle = <cfqueryparam cfsqltype="CF_SQL_VARCHAR" value="#arguments.ArtistSocialMediaHandle#" />
                , CurrentSocialMediaFollowers = <cfqueryparam cfsqltype="CF_SQL_INTEGER" value="#arguments.CurrentSocialMediaFollowers#" />
                , ReceiveDirectMessageNotifications = <cfqueryparam cfsqltype="CF_SQL_TINYINT" value="#arguments.ReceiveDirectMessageNotifications#" />
                , RecordLabel_ContactID = <cfqueryparam cfsqltype="CF_SQL_INTEGER" value="#arguments.RecordLabel_ContactID#" />
                , PublicistInfo = <cfqueryparam cfsqltype="CF_SQL_VARCHAR" value="#arguments.PublicistInfo#" />
                , RadioPromoTeamInfo = <cfqueryparam cfsqltype="CF_SQL_VARCHAR" value="#arguments.RadioPromoTeamInfo#" />
                , SongTitle = <cfqueryparam cfsqltype="CF_SQL_VARCHAR" value="#arguments.SongTitle#" />
                , TikTokAudioOptionID = <cfqueryparam cfsqltype="CF_SQL_INTEGER" value="#arguments.TikTokAudioOptionID#" />
                , TikTokAudioLinkURL = <cfqueryparam cfsqltype="CF_SQL_VARCHAR" value="#arguments.TikTokAudioLinkURL#" />
                , GenreIDList = <cfqueryparam cfsqltype="CF_SQL_VARCHAR" value="#arguments.GenreIDList#" />
                , KeyWordIDList = <cfqueryparam cfsqltype="CF_SQL_VARCHAR" value="#arguments.KeyWordIDList#" />
                , ReleaseDate = <cfqueryparam cfsqltype="CF_SQL_DATE" value="#arguments.ReleaseDate#" />
                , CampaignDurationID = <cfqueryparam cfsqltype="CF_SQL_INTEGER" value="#arguments.CampaignDurationID#" />
                , DestinationID = <cfqueryparam cfsqltype="CF_SQL_INTEGER" value="#arguments.DestinationID#" />
                , DestinationURL = <cfqueryparam cfsqltype="CF_SQL_VARCHAR" value="#arguments.DestinationURL#" />
                , DestinationURL_CurrentNumber = <cfqueryparam cfsqltype="CF_SQL_INTEGER" value="#arguments.DestinationURL_CurrentNumber#" />
                , Scrape_Results_URL = <cfqueryparam cfsqltype="CF_SQL_VARCHAR" value="#arguments.Scrape_Results_URL#" />
                , SocialMedia_UniqueID = <cfqueryparam cfsqltype="CF_SQL_VARCHAR" value="#arguments.SocialMedia_UniqueID#" />
                , URLErrorReasonID = <cfqueryparam cfsqltype="CF_SQL_INTEGER" value="#arguments.URLErrorReasonID#" />
                , ProxyCountryCode = <cfqueryparam cfsqltype="CF_SQL_VARCHAR" value="#arguments.ProxyCountryCode#" />
                , SocialMedia_ImageURL = <cfqueryparam cfsqltype="CF_SQL_VARCHAR" value="#arguments.SocialMedia_ImageURL#" />
                , BitlyLinkURL = <cfqueryparam cfsqltype="CF_SQL_VARCHAR" value="#arguments.BitlyLinkURL#" />
                , CampaignNotes_FromClient = <cfqueryparam cfsqltype="CF_SQL_VARCHAR" value="#arguments.CampaignNotes_FromClient#" />
                , Waiver1Agree = <cfqueryparam cfsqltype="CF_SQL_TINYINT" value="#arguments.Waiver1Agree#" />
                , Waiver2Agree = <cfqueryparam cfsqltype="CF_SQL_TINYINT" value="#arguments.Waiver2Agree#" />
                , MP3FileLocation = <cfqueryparam cfsqltype="CF_SQL_VARCHAR" value="#arguments.MP3FileLocation#" />
                , MP3URLLocation = <cfqueryparam cfsqltype="CF_SQL_VARCHAR" value="#arguments.MP3URLLocation#" />
                , AudioTimeStamp = <cfqueryparam cfsqltype="CF_SQL_VARCHAR" value="#arguments.AudioTimeStamp#" />
                , StartDate_ClientRequested = <cfqueryparam cfsqltype="CF_SQL_DATE" value="#arguments.StartDate_ClientRequested#" />
                , EndDate_ClientRequested = <cfqueryparam cfsqltype="CF_SQL_DATE" value="#arguments.EndDate_ClientRequested#" />
                , StartDate_Scheduled = <cfqueryparam cfsqltype="CF_SQL_DATE" value="#arguments.StartDate_Scheduled#" />
                , EndDate_Scheduled = <cfqueryparam cfsqltype="CF_SQL_DATE" value="#arguments.EndDate_Scheduled#" />
                , StartDate_Actual = <cfqueryparam cfsqltype="CF_SQL_DATE" value="#arguments.StartDate_Actual#" />
                , EndDate_Actual = <cfqueryparam cfsqltype="CF_SQL_DATE" value="#arguments.EndDate_Actual#" />
                , StartDate_UpdatedBy_ContactID = <cfqueryparam cfsqltype="CF_SQL_INTEGER" value="#arguments.StartDate_UpdatedBy_ContactID#" />
                , EndDate_UpdatedBy_ContactID = <cfqueryparam cfsqltype="CF_SQL_INTEGER" value="#arguments.EndDate_UpdatedBy_ContactID#" />
                , Campaign_FirstPost_DateTime = <cfqueryparam cfsqltype="CF_SQL_TIMESTAMP" value="#arguments.Campaign_FirstPost_DateTime#" />
                , Campaign_FirstPost_ClientNotified_DateTime = <cfqueryparam cfsqltype="CF_SQL_TIMESTAMP" value="#arguments.Campaign_FirstPost_ClientNotified_DateTime#" />
                , Campaign_FirstPost_ClientNotified_Email = <cfqueryparam cfsqltype="CF_SQL_VARCHAR" value="#arguments.Campaign_FirstPost_ClientNotified_Email#" />
                , CampaignViews_Target = <cfqueryparam cfsqltype="CF_SQL_INTEGER" value="#arguments.CampaignViews_Target#" />
                , NumberOfInfluencers_Target = <cfqueryparam cfsqltype="CF_SQL_VARCHAR" value="#arguments.NumberOfInfluencers_Target#" />
                , MinimumInflunecersTrigger = <cfqueryparam cfsqltype="CF_SQL_INTEGER" value="#arguments.MinimumInflunecersTrigger#" />
                , OnboardingCallDetails = <cfqueryparam cfsqltype="CF_SQL_VARCHAR" value="#arguments.OnboardingCallDetails#" />
                , CreativeDirectionNotes_FromStaff = <cfqueryparam cfsqltype="CF_SQL_VARCHAR" value="#arguments.CreativeDirectionNotes_FromStaff#" />
                , CreativeDirectionNotes_FromClient = <cfqueryparam cfsqltype="CF_SQL_VARCHAR" value="#arguments.CreativeDirectionNotes_FromClient#" />
                , PaymentStatusID = <cfqueryparam cfsqltype="CF_SQL_INTEGER" value="#arguments.PaymentStatusID#" />
                , PaymentStatus_UpdatedContactID = <cfqueryparam cfsqltype="CF_SQL_INTEGER" value="#arguments.PaymentStatus_UpdatedContactID#" />
                , PaymentStatus_UpdatedDateTime = <cfqueryparam cfsqltype="CF_SQL_TIMESTAMP" value="#arguments.PaymentStatus_UpdatedDateTime#" />
                , ClientPayment_InvoiceAmount = <cfqueryparam cfsqltype="CF_SQL_DECIMAL" value="#arguments.ClientPayment_InvoiceAmount#" />
                , ClientPayment_InvoiceDate = <cfqueryparam cfsqltype="CF_SQL_DATE" value="#arguments.ClientPayment_InvoiceDate#" />
                , ClientPayment_InvoiceNumber = <cfqueryparam cfsqltype="CF_SQL_VARCHAR" value="#arguments.ClientPayment_InvoiceNumber#" />
                , ClientPayment_PurchaseOrderNumber = <cfqueryparam cfsqltype="CF_SQL_VARCHAR" value="#arguments.ClientPayment_PurchaseOrderNumber#" />
                , ClientPayment_PaymentDate = <cfqueryparam cfsqltype="CF_SQL_DATE" value="#arguments.ClientPayment_PaymentDate#" />
                , ClientPayment_PaymentReferenceNumber = <cfqueryparam cfsqltype="CF_SQL_VARCHAR" value="#arguments.ClientPayment_PaymentReferenceNumber#" />
                , ClientPayment_PaymentMethodID = <cfqueryparam cfsqltype="CF_SQL_INTEGER" value="#arguments.ClientPayment_PaymentMethodID#" />
                , ClientPayment_BillingDetails = <cfqueryparam cfsqltype="CF_SQL_VARCHAR" value="#arguments.ClientPayment_BillingDetails#" />
                , ClientPayment_BillingNotes = <cfqueryparam cfsqltype="CF_SQL_VARCHAR" value="#arguments.ClientPayment_BillingNotes#" />
                , IsTest = <cfqueryparam cfsqltype="CF_SQL_TINYINT" value="#arguments.IsTest#" />
                , InfluencerStaffApproval_EmailSent_DateTime = <cfqueryparam cfsqltype="CF_SQL_TIMESTAMP" value="#arguments.InfluencerStaffApproval_EmailSent_DateTime#" />
                , InfluencerStaffApproval_EmailSent_To_ContactID = <cfqueryparam cfsqltype="CF_SQL_INTEGER" value="#arguments.InfluencerStaffApproval_EmailSent_To_ContactID#" />
                , InfluencerStaffApproval_EmailSent_From_ContactID = <cfqueryparam cfsqltype="CF_SQL_INTEGER" value="#arguments.InfluencerStaffApproval_EmailSent_From_ContactID#" />
                , StaffCommentsForClientReport = <cfqueryparam cfsqltype="CF_SQL_VARCHAR" value="#arguments.StaffCommentsForClientReport#" />
                , PostInvite_Expiry_Days = <cfqueryparam cfsqltype="CF_SQL_INTEGER" value="#arguments.PostInvite_Expiry_Days#" />
                , PostNotify_Expiry_Days = <cfqueryparam cfsqltype="CF_SQL_INTEGER" value="#arguments.PostNotify_Expiry_Days#" />
                , PostResults_NotBefore_Days = <cfqueryparam cfsqltype="CF_SQL_INTEGER" value="#arguments.PostResults_NotBefore_Days#" />
                , ArchiveCampaign_NotBefore_Days = <cfqueryparam cfsqltype="CF_SQL_INTEGER" value="#arguments.ArchiveCampaign_NotBefore_Days#" />
                , ArchiveCampaign_After_Date = <cfqueryparam cfsqltype="CF_SQL_DATE" value="#arguments.ArchiveCampaign_After_Date#" />
                , ScrapeResultsYesNo = <cfqueryparam cfsqltype="CF_SQL_TINYINT" value="#arguments.ScrapeResultsYesNo#" />
                , Chartmetric_ArtistID = <cfqueryparam cfsqltype="CF_SQL_VARCHAR" value="#arguments.Chartmetric_ArtistID#" />
                , Chartmetric_TrackID = <cfqueryparam cfsqltype="CF_SQL_VARCHAR" value="#arguments.Chartmetric_TrackID#" />
                , Chartmetric_AlbumID = <cfqueryparam cfsqltype="CF_SQL_VARCHAR" value="#arguments.Chartmetric_AlbumID#" />
                , Spotify_URL = <cfqueryparam cfsqltype="CF_SQL_VARCHAR" value="#arguments.Spotify_URL#" />
                , Score_ELO_UpdatedDateTime = <cfqueryparam cfsqltype="CF_SQL_TIMESTAMP" value="#arguments.Score_ELO_UpdatedDateTime#" />
                , SendCampaignReport = <cfqueryparam cfsqltype="CF_SQL_TINYINT" value="#arguments.SendCampaignReport#" />
                , InfluencerSpendPercent = <cfqueryparam cfsqltype="CF_SQL_DECIMAL" value="#arguments.InfluencerSpendPercent#" />
                , InfluencerSpendBudget = <cfqueryparam cfsqltype="CF_SQL_DECIMAL" value="#arguments.InfluencerSpendBudget#" />
                , InfluencerSpendBudgetNotes = <cfqueryparam cfsqltype="CF_SQL_VARCHAR" value="#arguments.InfluencerSpendBudgetNotes#" />
                , CompetitionOptionID = <cfqueryparam cfsqltype="CF_SQL_INTEGER" value="#arguments.CompetitionOptionID#" />
                , Competition_PrizeAmount = <cfqueryparam cfsqltype="CF_SQL_DECIMAL" value="#arguments.Competition_PrizeAmount#" />
                , Competition_Deadline = <cfqueryparam cfsqltype="CF_SQL_TIMESTAMP" value="#arguments.Competition_Deadline#" />
                , IsCarveoutCampaign = <cfqueryparam cfsqltype="CF_SQL_TINYINT" value="#arguments.IsCarveoutCampaign#" />
                , ShowInfluencerCountry = <cfqueryparam cfsqltype="CF_SQL_TINYINT" value="#arguments.ShowInfluencerCountry#" />
                , RequestBoostCode = <cfqueryparam cfsqltype="CF_SQL_TINYINT" value="#arguments.RequestBoostCode#" />
                , CreatedDateTime = <cfqueryparam cfsqltype="CF_SQL_TIMESTAMP" value="#arguments.CreatedDateTime#" />
                , CreatedContactID = <cfqueryparam cfsqltype="CF_SQL_INTEGER" value="#arguments.CreatedContactID#" />
                , ModifiedDateTime = <cfqueryparam cfsqltype="CF_SQL_TIMESTAMP" value="#arguments.ModifiedDateTime#" />
                , ModifiedContactID = <cfqueryparam cfsqltype="CF_SQL_INTEGER" value="#arguments.ModifiedContactID#" />
            WHERE
                CampaignID = <cfqueryparam cfsqltype="CF_SQL_INTEGER" value="#arguments.CampaignID#" />
        </cfquery>

    <cfreturn local.qInsert.generatedKey />
    </cffunction>

</cfcomponent>